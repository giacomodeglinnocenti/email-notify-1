# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.1.0

- minor: Initial release

